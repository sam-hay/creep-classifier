#!/usr/bin/env python3
from random import randint
import argparse
import csv

def create_random_goldstandard(inputfilename,outputfilename):
    row1_labels=['OTHER','OFFENSE']
    row2_labels=['OTHER','ABUSE','INSULT','PROFANITY']
    infile=open(inputfilename,'r')
    outfile=open(outputfilename,'w')
    data=[]
    csv_writer=csv.writer(outfile, delimiter='\t')
    for line in infile.readlines():
        if(line[-1]=='\n'):
            curr_line=line[:-1]
        else:
            curr_line=line
        label1_random=row1_labels[randint(0,len(row1_labels)-1)]
        label2_random=row2_labels[randint(0,len(row2_labels)-1)]
        csv_writer.writerow([curr_line, label1_random, label2_random])

    outfile.close()


if(__name__=='__main__'):
    parser=argparse.ArgumentParser()
    parser.add_argument('infile', action='store')
    parser.add_argument('outfile', action='store')
    args=parser.parse_args()

    create_random_goldstandard(args.infile, args.outfile)
    
