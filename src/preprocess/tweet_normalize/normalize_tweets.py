from .ekphrasis.classes.preprocessor import TextPreProcessor
from .ekphrasis.classes.tokenizer import SocialTokenizer
from .ekphrasis.dicts.emoticons import emoticons
import re
import diff_match as dmp_module
import os

curr_script_folder='/'.join(os.path.relpath(__file__).split('/')[:-1])
ngramsFolder="{}/ekphrasis/stats/german_nolc/".format(curr_script_folder)

text_processors_multilingual={}

#capitalized german word frequencies dict
deDict_nolc=dict()
with open(ngramsFolder+"counts_1grams.txt") as f:
    content = f.readlines()
    for l in content:
        line = l.rstrip().split("\t")
        deDict_nolc[line[0]]=line[1]

def capitalizeText(tokenizedText):
    wordList = tokenizedText.split(" ")
    capitalizedText=""
    for w in wordList:
        #lowercase freq
        if w in deDict_nolc:
            lc_freq=int(deDict_nolc[w])
        else:
            lc_freq=0
        #uppercase freq
        if w.capitalize() in deDict_nolc:
            uc_freq=int(deDict_nolc[w.capitalize()])
        else:
            uc_freq=0
        if lc_freq > uc_freq:
            capitalizedText=capitalizedText+w+" "
        else:
            capitalizedText=capitalizedText+w.capitalize()+" "  
    return(capitalizedText)

def removeEndingElongated (text):
    text = re.sub(r'[a][a]+([^A-Za-z])', 'a\g<1>', text)
    text = re.sub(r'[a][a]+$', 'a', text)
    text = re.sub(r'[e][e]+([^A-Za-z])', 'e\g<1>', text)
    text = re.sub(r'[e][e]+$', 'e', text)
    text = re.sub(r'[i][i]+([^A-Za-z])', 'i\g<1>', text)
    text = re.sub(r'[i][i]+$', 'i', text)
    text = re.sub(r'[o][o]+([^A-Za-z])', 'o\g<1>', text)
    text = re.sub(r'[o][o]+$', 'o', text)
    text = re.sub(r'[u][u]+([^A-Za-z])', 'u\g<1>', text)
    text = re.sub(r'[u][u]+$', 'u', text)
    return(text)


def normalizeInput (lang, text):
    if lang == "en":
        normalizedText = normalize_en(text)
    if lang == "it":
        text = removeEndingElongated(text)
        normalizedText = normalize_it(text)
    if lang == "de":
        normalizedText = normalize_de(text)
        normalizedText = capitalizeText(normalizedText)
    return(normalizedText)

def cleanString(text):
    regex = re.compile('[^A-Za-z ]')
    return(regex.sub('', text))


def checkUpperCase(inputarray):
    
    fistString=str(inputarray[0][1])
    fistString=cleanString(fistString)

    if len(inputarray)>1:
        secondString=str(inputarray[1][1])
        returnString=""
        
        if inputarray[0][0] == -1 and inputarray[1][0] == 1:

            if re.match(r'\s', secondString):
                addSpace=True
                returnString=returnString+" "
                secondString=secondString.lstrip()

            if len(fistString) == 1 and len(secondString) == 1:
                if fistString.lower() == secondString.lower():
                    returnString=returnString+fistString
                    return(returnString)
                else:
                    returnString=returnString+secondString
                    return returnString
            else:
                returnString=returnString+secondString
                return(returnString)
    else:
        if inputarray[0][0] == 1:
            return(fistString)
        else:
            return("")



def restoreUpperCase(text1, text2):

    dmp = dmp_module.diff_match_patch()
    diff = dmp.diff_main(text1, text2)
    dmp.diff_cleanupSemantic(diff)

    betweenzerolist=[]
    restoredString=""
    if len(diff) == 1:
        restoredString = diff[0][1]

    else:   
        for d in diff:
            if d[0] is not 0:
                betweenzerolist.append(d)

            else:
                if len(betweenzerolist)>0:
                    stringToPrint = checkUpperCase(betweenzerolist)
                    restoredString=restoredString+str(stringToPrint)
                    betweenzerolist=[]
                    restoredString=restoredString+str(d[1])
                else:
                    restoredString=restoredString+d[1]


        if len(betweenzerolist)>0:
            stringToPrint = checkUpperCase(betweenzerolist)
            restoredString=restoredString+str(stringToPrint)

    return(restoredString)



def normalize_en (text):
    if('en' not in text_processors_multilingual):
        text_processors_multilingual['en'] = TextPreProcessor(
            # normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
            #     'time', 'url', 'date', 'number'],
            normalize=[],
            # annotate={"hashtag", "allcaps", "elongated", "repeated",
            #     'emphasis', 'censored'},
            fix_html=True,  
            segmenter="english", 
            corrector="english", 
            unpack_hashtags=True, 
            unpack_contractions=False,  
            spell_correct_elong=False,  
            spell_correction=False,
            tokenizer=SocialTokenizer(lowercase=True).tokenize,
        )
    
    text_processor_en=text_processors_multilingual['en']
    textprocessed = (" ".join(text_processor_en.pre_process_doc(text)))
    textprocessedUppercase = restoreUpperCase(text,textprocessed)
    return(textprocessedUppercase)

def normalize_it (text):
    if('it' not in text_processors_multilingual):
        text_processors_multilingual['it'] = TextPreProcessor(
            # normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
            #     'time', 'url', 'date', 'number'],
            normalize=[],
            # annotate={"hashtag", "allcaps", "elongated", "repeated",
            #     'emphasis', 'censored'},
            fix_html=False,  
            segmenter="italian", 
            corrector="italian", 
            unpack_hashtags=True, 
            unpack_contractions=False,  
            spell_correct_elong=False,  
            spell_correction=False,
            tokenizer=SocialTokenizer(lowercase=True).tokenize,
        )

    text_processor_it=text_processors_multilingual['it']
    textprocessed = (" ".join(text_processor_it.pre_process_doc(text)))
    textprocessedUppercase = restoreUpperCase(text,textprocessed)
    return(textprocessedUppercase)
    
def normalize_de (text):
    if('de' not in text_processors_multilingual):
        text_processors_multilingual['de'] = TextPreProcessor(
            # normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
            #     'time', 'url', 'date', 'number'],
            normalize=[],
            # annotate={"hashtag", "allcaps", "elongated", "repeated",
            #     'emphasis', 'censored'},
            fix_html=True,  
            segmenter="german_lc", 
            corrector="german_lc", 
            unpack_hashtags=True, 
            unpack_contractions=False,  
            spell_correct_elong=False,  
            spell_correction=False,
            tokenizer=SocialTokenizer(lowercase=True).tokenize,
        )

    text_processor_de=text_processors_multilingual['de']
    text = re.sub(r'(#[A-Z][a-z]+[^A-Za-z])', lambda pat: pat.group(1).lower(), text)
    text = re.sub(r'(#[A-Z][a-z]+$)', lambda pat: pat.group(1).lower(), text)
    return(" ".join(text_processor_de.pre_process_doc(text)))



# ESEMPI
# qui lo lancio sugli esempi che ho messo prima in examples_de/it/en per provare se funziona
# per lanciarlo sui nostri dati si usa
# normalizeInput(lang, text)
# lang puo' essere en/de/it

if(__name__=='__main__'):

    examples_de = [
        "#AusDemNichts",
        "#Deutschlandhassern",
        "#Dieselgipfel",
        "#Drehhofer",
        "#EUBeitritt",
        "#Europawahn",
        " the #Europawahn is good",
        "#FatihAkin",
        "#GoldenGlobes2017",
        "This is #GoldenGlobes2017",
        "#HamburgPride",
        "#JamaikaAbbruch",
        "#KardinalMarx",
        "#LandderVollidioten",
        "#LandshuterHochzeit",
        "#Migrationsgesetz",
        "#Synodalvertretung",
        "#TaufedesHerrn",
    ]
    examples_it = [
        "#canaledisicilia",
        "#lagabbia",
        "ma ciaooo!",
        "ruspaaa",
        "#muorifrocio",
        "#bruttamerda",
        "#copritivacca",
        "noooooo, che succede?",
        "#cazzono:(",
        "#esiamotuttifelici",
        "#cambiaverso",
        "#Alfano:Stop",
        "che palleeee",
        "#USA,anche",
        "#puntidivista",
        "#HateSpeech",
        "#mattino5",
        "#bastardiislamici",
        "#calcinculo",
        "#portaaporta",
        "#terroristiislamici"
    ]
    examples_en = [
        "CANT WAIT #toobad for the new season of #TwinPeaks ＼(^o^)／!!! #davidlynch #tvseries :)))",
        "I saw the new #johndoe movie and it suuuuucks!!! WASTED $10... #badmovies :/",
        "@SentimentSymp:  can't wait for the Nov 9 #Sentiment talks!  YAAAAAAY !!! :-D http://sentimentsymposium.com/. I love you"
    ]


    lang="de"
    for e in examples_de:
        text=e
        print("Originale:    "+e)
        textnorm = normalizeInput(lang, text)
        print("Normalizzata: "+textnorm)
        print()

    lang="it"
    for e in examples_it:
        text=e
        print("Originale:    "+e)
        textnorm = normalizeInput(lang, text)
        print("Normalizzata: "+textnorm)
        print()

    lang="en"
    for e in examples_en:
        text=e
        print("Originale:    "+e)
        textnorm = normalizeInput(lang, text)
        print("Normalizzata: "+textnorm)
        print()


