#!/usr/bin/python3

import numpy as np

from model.models import ModelClassifier
from model.model_configuration import get_number_configurations
from model.model_configuration import configuration_generator
from preprocess.feature_extraction import get_social_features, get_all_features, tokenize_normalize_only
from train_test.sequences import TestSequence
from metrics.performance_metrics import get_performance_metrics


def test_validation_set(x_val, y_val, config_filename,
                        fields, training_epochs, x_train):
    """test multiple models with different configurations and epochs 
    over the validation set, and selects the best

    Args:
        x_val: validation data (no feature extraction done)
        y_val: validation labels
        config_filename: path for the model configuration file
        fields: the labels for the current dataset
        training epochs: number of training epochs
        x_train: training data (no feature extraction done)

    Returns:
        the best performing model index and metrics
    """
    
    num_configurations=get_number_configurations(config_filename)
    
    model_results=np.zeros((num_configurations, training_epochs, len(fields)+2,
                            4))
    best_fscore = None

    conf_generator=configuration_generator(config_filename)
    
    #test every configuration and store the result
    for i in range(num_configurations):
        model_conf=next(conf_generator)
        
        model_conf.fit_all_vectorizers(x_train) 

        epoch_range=range(0,training_epochs)
        curr_metrics,_,_ = test_network(x_val, y_val, fields, 
                                        model_conf, epoch_range)
        model_results[i,:]=curr_metrics[:]
        for j in epoch_range:
            if(best_fscore is None or curr_metrics[j,-2, 2] >= best_fscore):
                best_fscore = curr_metrics[j,-2,2]
                best_model_idx = (i,j)

    return best_model_idx, model_results

def validate_from_single_config(x_val, y_val, model_conf, fields, training_epochs):

    model_results=np.zeros((1, training_epochs, len(fields)+2, 4))
    epoch_range=range(0,training_epochs)
    curr_results,_,_ = test_network(x_val, y_val, fields, 
                                  model_conf, False, epoch_range)

    model_results[0,:]=curr_results[:]
    best_fscore=None
    count_stale=0
    for j in epoch_range:
        if(best_fscore is None or curr_results[j,-2, -2] >= best_fscore):
            best_fscore = curr_results[j,-2,-2]
            best_model_idx = (0,j)

    return best_model_idx, model_results 


def test_network(x_test, y_test, fields, model_conf, epoch_range=None):
    """Test a single model configuration, either over multiple epochs
       (if epoch_range is not None) or the best performing one

    Args:
        x_test: test data (no feature extraction done)
        y_test: test labels
        fields: dataset labels
        model_conf: current ModelConfiguration
        epoch_range: optional, an epoch value for the model, if epoch_range
                     is None, we use the (previously renamed) 
                     best model from validation

    Returns:
        Performance and confusion matrices for the tested models
    """

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import precision_recall_fscore_support
    import json
    # test the best performing network
    
    if(epoch_range is None):
        import pickle
        fname=model_conf.get_file_name('best', True)
        print('the best model is {}'.format(str(model_conf)))
        epoch_range=range(1)
        test_only=True
    else:
        fname = model_conf.get_file_name()
        test_only=False
    
    if(model_conf.model_type=='svm' and len(fields)==2):
        y_labels=np.where(y_test==1, 1, -1)
    else:
        y_labels=y_test
    
    batch_size=128
    
    # turns out the generator is actually faster for lstms
    use_generator=("char-rnn" in model_conf.text_features or
                   "insta2vec" in model_conf.text_features)

    #get features for testing without generator
    if(not use_generator):
        try:
            test_vector=get_all_features(x_test, model_conf) 
    
        except MemoryError:
            print('could not fit the entire dataset in memory, using generator '
                  '(sequence)')
            use_generator=True
    
    performance_metrics=np.zeros((len(epoch_range),len(fields)+2,4))
    confusion_matrices=np.zeros((len(epoch_range),len(fields),len(fields)))
    
    model = None
    if(len(fields)>2):
        output_size=len(fields)
    else:
        output_size=1
    
    #test over multiple epochs
    for epoch in epoch_range:
     
        weights_filename=fname.format(epoch=epoch+1)
        print('testing model {}'.format(weights_filename))

        model = ModelClassifier.from_file(model_conf, output_size,
                                          weights_filename)

        #test without generator
        if(not use_generator):
            predictions=model.predict(test_vector,batch_size=batch_size)

        #test with generator
        else:
            generator = TestSequence(x_test, model_conf)
            predictions = model.predict_generator(generator)

        if(model_conf.attention and test_only):
            tokenized_list=tokenize_normalize_only(x_test, model_conf)
            predict_attention=model.predict_attention(test_vector, batch_size)
            attention_output=[]
            for i in range(len(tokenized_list)):
                curr_tweet={}
                curr_tweet['words']=tokenized_list[i]
                curr_tweet['attention']=predict_attention[i].tolist()
                attention_output.append(curr_tweet)

            attention_filename='../results/attention-{}.json'.format(model_conf.get_file_name().split('/')[-1])
            attention_file=open(attention_filename, 'w')
            json.dump(attention_output, attention_file, indent=4, ensure_ascii=False)
            attention_file.close()

        #deallocate the model, freeing up space 
        del(model)

        performance, confusion=get_performance_metrics(fields, predictions,
                                                      model_conf, y_test)
        performance_metrics[epoch] = performance

        confusion_matrices[epoch] = confusion


    return performance_metrics, confusion_matrices, predictions

def label_unlabeled_file(rank_filename, model_conf, data, text_field_no, fields):
    import csv
    import random
    from preprocess.feature_extraction import get_emolex


    if(len(fields)>2):
        output_size=len(fields)
    else:
        output_size=1

    model_name=model_conf.get_file_name('best', True)
    model=ModelClassifier.from_file(model_conf, output_size, model_name)

    batch_size=256
    model_conf.batch_size=batch_size

    use_generator=False
    print('not using generator')
    if(use_generator):
        print('using generator')
        generator = TestSequence(data, model_conf)
        predictions=model.predict_generator(generator)
    else:
        x_train=get_all_features(data, model_conf)
        predictions=model.predict(x_train, batch_size=64)
    
    predictions_binary=np.round(predictions)

    rank_filename=rank_filename.split('/')[-1]
    output_filename='labeled-{}'.format(rank_filename)

    csv_outfile=open(output_filename,'w')
    csv_writer=csv.writer(csv_outfile, delimiter='\t')
    for idx, post in enumerate(data):
        row=post['other_fields']
        row.insert(text_field_no, post['text'])
        row+=[fields[int(predictions_binary[idx])]]
        csv_writer.writerow(row)

    csv_outfile.close()
    return
