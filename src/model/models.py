import math
import os
from keras.models import Model, load_model
from keras.layers import Input, Dense, Masking, Dropout, Layer
from keras.layers import BatchNormalization, Bidirectional
from keras.layers import concatenate, Embedding, TimeDistributed, Multiply
from keras.layers import Conv1D, LSTM, GRU, MaxPooling1D, Softmax, Lambda
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.callbacks import TensorBoard
from keras import regularizers
from keras.optimizers import SGD
from keras import backend as K

from train_test.sequences import TrainSequence, TrainLanguageModelSequence
from model.model_configuration import ModelConfiguration

class GenericModel():

    def __init__(self, model_conf):
        self.model_conf=model_conf
        self.regularizer=regularizers.l2(model_conf.regularization_lambda)
        self.input_list=[]
        self.output=None


    def fit(self, x_train, y_train, epochs, batch_size,
                  initial_epoch=1, validation_split=0.0):
        self.model.fit(x_train, y_train, epochs=epochs,
                  initial_epoch=initial_epoch, batch_size=batch_size,
                  callbacks=self.callback_list, validation_split=validation_split )


    def fit_generator(self, generator, epochs, initial_epoch=1,
                      validation_data=None):
        self.model.fit_generator(generator, epochs=epochs,
                                 callbacks=self.callback_list,
                                 shuffle=True,
                                 validation_data=validation_data,
                                 initial_epoch=initial_epoch)


    def predict(self, x_test, batch_size):
        return self.model.predict(x_test, batch_size=batch_size,
                                  verbose=1)

    def predict_attention(self, x_test, batch_size):
        return self.model_attention_outputs.predict(x_test,
                                                    batch_size=batch_size,
                                                    verbose=1)


    def predict_generator(self, generator):
        import multiprocessing
        import socket

        use_gpu = K.tensorflow_backend._get_available_gpus() > 0

        if(use_gpu):
            cpu_count = multiprocessing.cpu_count()*2
            return self.model.predict_generator(generator,
                                                use_multiprocessing=True,
                                                max_queue_size=20,
                                                workers=cpu_count-2,
                                                verbose=1)
        else:
            return self.model.predict_generator(generator,
                                                use_multiprocessing=True,
                                                verbose=1)



    def load_model(self, filename, weights_only=False, by_name=True):
        if(weights_only):
            self.model.load_weights(filename, by_name=by_name)
        else:
            self.model = load_model(filename)

        if(self.model_conf.attention):
            self.model_attention_outputs.load_weights(filename, by_name=True)

    def get_dense_layer(self, input_tensor, size, dropout_fraction=0,
                        activation_function='relu', use_batch_norm=None,
                        layer_wrapper=None, name=None):

        if(layer_wrapper is None):
            layer_wrapper = lambda x : x

        dense = layer_wrapper(Dense(size, activation=activation_function,
                                    kernel_regularizer=self.regularizer,
                                    name=name))(input_tensor)

        if(use_batch_norm is None):
            batch_norm=self.model_conf.use_batch_normalization

        if(use_batch_norm):
            dense=BatchNormalization(name='batchnorm_'+name)(dense)

        if(dropout_fraction > 0):
            dense=Dropout(dropout_fraction, name='dropout_'+name)(dense)

        return dense

    def get_embeddings_layer(self, input_tensor, dict_size, output_size, 
                             mask_zero=False, name=None):
        return Embedding(dict_size, output_size,
                         embeddings_regularizer=self.regularizer,
                         mask_zero=mask_zero,
                         name=name)(input_tensor)


    def get_input_layer(self, input_size, dropout_fraction=0, mask_value=None,
                        name=None, dropout_shape=None):

        if(isinstance(input_size, tuple)):
            input_tensor=Input((input_size),name=name)
            self.input_list.append(input_tensor)
        else:
            curr_input_list=[]
            for idx,size in enumerate(input_size):
                input_tensor=Input((size),name=name[idx])
                curr_input_list.append(input_tensor)
            self.input_list+=curr_input_list
            input_tensor=concatenate(curr_input_list)
            name='concatenate_inputs'+'_'.join(name)

        if(mask_value is not None):
            input_tensor=Masking(mask_value,name='masking_'+name)(input_tensor)

        if(dropout_fraction > 0):
            input_tensor=Dropout(dropout_fraction,
                                 noise_shape=dropout_shape,
                                 name='dropout_'+name)(input_tensor)
        return input_tensor

    def get_recurrent_layer(self, input_tensor, return_sequences=False, sizes=None, 
                            layer_type=None, layer_wrapper=None, name=None):

        if(sizes is None):
            sizes=self.model_conf.recurrent_layer_sizes

        if(layer_type is None):
            layer_type = self.model_conf.recurrent_layer_type

        if(layer_wrapper is None):
            layer_wrapper = lambda x : x

        recurrent_dropout=self.model_conf.recurrent_dropout
        regularizer=self.regularizer

        curr_layer = input_tensor

        for idx, size in enumerate(sizes):
            last_layer=(idx == (len(sizes)-1))
            return_sequences_curr= (not last_layer) or return_sequences
            if(layer_type == 'LSTM'):
                layer = LSTM(size, return_sequences=return_sequences_curr,
                             recurrent_dropout=recurrent_dropout,
                             kernel_regularizer=regularizer,
                             name=name+str(idx))
            elif(layer_type == 'GRU'):
                layer = GRU(size, return_sequences=return_sequences_curr,
                           recurrent_dropout=recurrent_dropout,
                           kernel_regularizer=regularizer,
                           name=name+str(idx)) 
            elif(layer_type == 'BiLSTM'):
                layer = Bidirectional(LSTM(size, return_sequences=return_sequences_curr,
                             recurrent_dropout=recurrent_dropout,
                             kernel_regularizer=regularizer,
                             name=name+str(idx)))
            elif(layer_type == 'CNN+GRU'):
                #FIXME this was never tested
                layer = GRU(size, return_sequences=return_sequences)\
                            (MaxPooling1D(4)(Conv1D(100, 4, padding='same',
                                                    activation='relu')))

            curr_layer = layer_wrapper(layer)(curr_layer)
        return curr_layer

    def disable_layers(self, epoch, transfer_type='language'):
        if('trainable_layers' not in self.__dict__ or 
           self.trainable_layers is None):
            self.trainable_layers=[]
            for layer in self.model.layers:
                if(len(layer.trainable_weights)>0):
                    layer.trainable=False
                    self.trainable_layers.append(layer)
            if(transfer_type=='language'):
                self.trainable_layers.reverse()

        for layer in self.trainable_layers[:epoch+1]:
            print('layer {} enabled'.format(layer.name))
            layer.trainable=True

        return

    def count_layers(self):

        trainable_layers=[]
        for layer in self.model.layers:
            if(len(layer.weights)>0):
                trainable_layers.append(layer)

        return len(trainable_layers)

    def enable_single_layer(self, layer_idx):
        if('trainable_layers' not in self.__dict__ or 
           self.trainable_layers is None):
            self.trainable_layers=[]
            for layer in self.model.layers:
                if(len(layer.weights)>0):
                    self.trainable_layers.append(layer)

        for layer in self.trainable_layers:
            layer.trainable=False

        print('enabling layer {}'.format(layer_idx))
        self.trainable_layers[layer_idx].trainable=True

        return


    def __del__(self):
        import keras.backend as K
        K.clear_session()


class MaskSoftmax(Layer):
    def __init__(self, mask=None):
        super().__init__()
        if(mask is not None):
            self.supports_masking = True
        self.mask=mask

    def compute_output_shape(self, input_shape):
        return input_shape

    def compute_mask(self, inputs, mask=None):
        if callable(self.mask):
            return self.mask(inputs, mask)
        return self.mask

    def call(self, inputs, mask=None):
        exponents=K.exp(inputs)
        masked_exponents=exponents*K.cast(K.expand_dims(mask), K.dtype(inputs))
        masked_sums=K.sum(masked_exponents, axis=1, keepdims=True)
        return (masked_exponents / masked_sums)

class ModelClassifier(GenericModel):

    def __init__(self, model_conf, output_size, model_filename=None):
        super().__init__(model_conf)
        self.output_size=output_size
        self.dropout_count=0
        self.create_model()
        if(model_filename is not None):
            super().load_model(model_filename, True, False)
        self.build_model()
        self.populate_callbacks()

    @classmethod
    def from_file(cls, model_conf, output_size,model_filename):
        return cls(model_conf, output_size, model_filename)

    def populate_callbacks(self, callbacks=None):
        if(callbacks is None):
            save_callback = ModelCheckpoint(self.model_conf.get_file_name(),
                                            save_best_only=False,
                                            save_weights_only=True)

            self.callback_list = [save_callback]
        else:
            self.callback_list=callbacks

    def fit(self, x_train, y_train, epochs, initial_epoch):
        super().fit(x_train, y_train, epochs, self.model_conf.batch_size, 
                    initial_epoch)

    def fit_generator(self, data, labels, epochs, initial_epoch=1):
        train_generator = TrainSequence(data, labels, self.model_conf,
                                        initial_epoch)
        super().fit_generator(train_generator, epochs=epochs, 
                              initial_epoch=initial_epoch)

    def get_curr_dropout_fraction(self):
        if(self.dropout_count < len(self.model_conf.dropout)):
            dropout_fraction = self.model_conf.dropout[self.dropout_count]
            self.dropout_count+=1
        else:
            dropout_fraction=0
        return dropout_fraction

    def create_model(self):
        self.create_feature_layers()

        # all the previous features, if enabled, are concatenated
        if(len(self.concatenate_inputs)>1):
            merged = concatenate(self.concatenate_inputs, 
                                 name='concatenate_all_features')
        else:
            merged=self.concatenate_inputs[0]

        output_hidden = self.create_hidden_layer(merged)

        self.create_output_layer(output_hidden, self.output_size)
        self.model=Model(inputs=self.input_list, outputs=self.output)

        if(self.model_conf.attention):
            self.model_attention_outputs=Model(inputs=self.input_list,
                                               outputs=self.attention_outputs)

    def disable_layers(self, epoch, transfer_type='language'):
        super().disable_layers(epoch, transfer_type)
        self.build_model()


    def enable_single_layer(self, layer_idx):
        super().enable_single_layer(layer_idx)
        self.build_model()

    def build_model(self):
        # apply the appropriate activation function to the output based on the model and number of output
        if(self.output_size == 1):
            if(self.model_conf.model_type=='svm'):
                loss_function='squared_hinge'
            else:
                loss_function='binary_crossentropy'

            self.model.compile(optimizer='adam',
                          loss=loss_function,
                          metrics=['accuracy'])
        else:
                if(self.model_conf.model_type=='svm'):
                    loss_function='categorical_hinge'
                else:
                    loss_function='categorical_crossentropy'

                self.model.compile(optimizer='adam',
                              loss=loss_function,
                              metrics=['accuracy'])
        self.model.summary()

        if(self.model_conf.attention):
            self.model.compile(optimizer='adam', loss='mean_squared_error')

    def create_hidden_layer(self,merged):
        curr_layer=merged
        for idx,layer_size in enumerate(self.model_conf.hidden_layers):
            dropout_fraction = self.get_curr_dropout_fraction()
            layer_name="hidden{}".format(idx)
            dense = self.get_dense_layer(curr_layer, layer_size, 
                                         dropout_fraction, name=layer_name)

            curr_layer=dense

        return curr_layer

    def create_output_layer(self,input_tensor, output_size):
        if(output_size == 1):
            if(self.model_conf.model_type=='svm'):
                activation_function='linear'
            else:
                activation_function='sigmoid'

        else:
            if(self.model_conf.model_type=='nnet'):
                activation_function='softmax'
            else:
                activation_function='sigmoid'

        self.output = self.get_dense_layer(input_tensor, output_size,
                                            activation_function=activation_function,
                                            use_batch_norm=False)

    def create_feature_layers(self):
        dropout=self.model_conf.dropout
        recurrent_dropout=self.model_conf.recurrent_dropout

        self.concatenate_inputs=[]

        # Inputs, an LSTM and inputs for sentiment values and some social features
        if('word2vec' in self.model_conf.text_features):
            # Masking layer, it removes the padding from the sequences before they
            # are propagated to the LSTM
            if(self.model_conf.train_dataset_name=='instagram-cyberbullying'):
                input_shapes=[(None, self.model_conf.embedding_size),
                              (None, 197)]
                dropout_fraction=self.get_curr_dropout_fraction()
                input_layer = self.get_input_layer(input_shapes, 
                                                   dropout_fraction,-1.,
                                                   dropout_shape=(None,None,1),
                                                   name='word2vec_input')

                lstm_out=self.get_recurrent_layer(input_layer, False,
                                                  name='word2vec_rec_layer')

            else:
                use_attention=self.model_conf.attention

                input_shapes=(None, self.model_conf.embedding_size)
                dropout_fraction = self.get_curr_dropout_fraction()
                input_layer = self.get_input_layer(input_shapes,
                                                   dropout_fraction,-1.,
                                                   dropout_shape=(None,None,1),
                                                   name='word2vec_input')


                # A recurrent layer to encode word embedding sequences
                lstm_out=self.get_recurrent_layer(input_layer, 
                                                  return_sequences=use_attention,
                                                  name='word2vec_rec_layer')

                if(use_attention):
                    attention_vector=TimeDistributed(Dense(1, activation='tanh'))(lstm_out)
                    attention_softmax=MaskSoftmax()(attention_vector)
                    self.attention_outputs=attention_softmax
                    attended_outputs=Multiply()([lstm_out, attention_softmax])
                    lstm_out=Lambda(lambda x: K.sum(x, axis=1))(attended_outputs)

            self.concatenate_inputs.append(lstm_out)

        # simple char based rnn
        if('char-rnn' in self.model_conf.text_features):
            char_input=self.get_input_layer((None,),name='input_char_rnn')

            embedding_char = self.get_embeddings_layer(char_input,
                                                       self.model_conf.char_count+1,
                                                       10,
                                                       name='embedding_char_rnn')
            layer_wrapper= lambda x : Bidirectional(x,name='bidirectional_char_rnn')

            lstm_out=self.get_recurrent_layer(embedding_char, False,400, 'LSTM',
                                              layer_wrapper)

            self.concatenate_inputs.append(lstm_out)

        # average and sum of word2vec embeddings
        if('cbows' in self.model_conf.text_features):
            cbows_input_sum=self.get_input_layer((self.model_conf.embedding_size,),
                                                 name='input_cbows_sum')
            cbows_input_avg=self.get_input_layer((self.model_conf.embedding_size,),
                                                 name='input_cbows_avg')

            self.concatenate_inputs.append(cbows_input_sum)
            self.concatenate_inputs.append(cbows_input_avg)

        # unigrams and/or bigrams
        if(self.model_conf.use_ngrams()):
            ngrams_vocab_size=len(self.model_conf.ngram_vectorizer.get_feature_names())

            input_ngrams=self.get_input_layer((ngrams_vocab_size,),
                                              name='input_ngrams')

            self.concatenate_inputs.append(input_ngrams)

        # pos unigrams and/or bigrams
        if(self.model_conf.use_pos()):
            pos_vocab_size=len(self.model_conf.pos_vectorizer.get_feature_names())
            input_pos=self.get_input_layer((pos_vocab_size,),
                                           name='input_pos')

            self.concatenate_inputs.append(input_pos)

        if('emolex' in self.model_conf.other_features):
            input_emolex=self.get_input_layer((10,),name='input_emolex')

            self.concatenate_inputs.append(input_emolex)

        if('hurtlex' in self.model_conf.other_features):
            input_hurtlex=self.get_input_layer((19,),name='input_hurtlex')

            self.concatenate_inputs.append(input_hurtlex)



        #only for instagram-cyberbullying
        if('emotion' in self.model_conf.other_features):
            input_emotion=self.get_input_layer((None,5),name='input_emotion')

            emotion_lstm=self.get_recurrent_layer(input_emotion, False, 50,
                                                  name='emotion_rec_layer')
            self.concatenate_inputs.append(emotion_lstm)

        #sentiment compound values
        if('sentiment' in self.model_conf.other_features):
            if(self.model_conf.dataset_name=='instagram-cyberbullying'):
                sentiment_input = self.get_input_layer((None,1),
                                                       name='input_sentiment')

                sentiment_input = self.get_recurrent_layer(sentiment_input,
                                                           False, 10,
                                                           name='sentiment_rec_layer')
            else:
                sentiment_input = self.get_input_layer((1,),name='sentiment_input')

            self.concatenate_inputs.append(sentiment_input)

        # social features (emojis, etc)
        if('social' in self.model_conf.other_features):
            use_num_posts=int(self.model_conf.dataset_name=='instagram-cyberbullying')
            social_input=self.get_input_layer((6+use_num_posts,),name='input_social')

            self.concatenate_inputs.append(social_input)

        if('img_creender_predictions' in self.model_conf.other_features):
            creender_predictions_input=self.get_input_layer((1,), 
                                                            name='input_creender_predictions')
            self.concatenate_inputs.append(creender_predictions_input)

        if("img_creender_descriptors" in self.model_conf.other_features):
            creender_descriptors_input=self.get_input_layer((2048,), 
                                                            name='input_creender_descriptors')
            self.concatenate_inputs.append(creender_descriptors_input)

        if("img_sentiment_predictions" in self.model_conf.other_features):
            sentiment_predictions_input=self.get_input_layer((1,), 
                                                            name='input_sentiment_predictions')
            self.concatenate_inputs.append(sentiment_predictions_input)

        if("img_sentiment_descriptors" in self.model_conf.other_features):
            sentiment_descriptors_input=self.get_input_layer((2048,), 
                                                            name='input_sentiment_descriptors')
            self.concatenate_inputs.append(sentiment_descriptors_input)
