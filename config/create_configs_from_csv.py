#!/usr/bin/env python3
import csv
import json
import argparse
import numpy as np
import copy
import os

base_config={'all_layer_sizes':[[200]],
             'all_recurrent_layer_sizes': [[100]],
             'all_lambdas' : [0],
             'all_batch_sizes' : [32],
             'all_dropouts' : [[]],
             'all_recurrent_dropouts' : [0.2],
             'all_use_batch_normalization' : [False],
             'all_model_types': ['nnet']
             }

dataset_map={'evalita facebook':'evalita-facebook',
             'evalita twitter': 'evalita-twitter',
             'evalita twitter+facebook' : 'evalita-both',
             'evalita twitter + whatsapp': 'whatsapp+evalita-twitter',
             'twitter+facebook+instagram+whatsapp' : 'twitter+facebook+instagram+whatsapp'
}

word2vec_source_map={'-' : None,
                     'EN_IT (aligned_pivot)' : ['multilingual-pivot'],
                     'fastext ita' : ['fasttext-it'],
                     'twitter_ita?' : ['fasttext-twitter-it']}

text_features_map={'Unigrams':[['unigrams']], 'Bigrams': [['bigrams']],
                   'Unigrams + Bigrams':[['unigrams','bigrams']],
                   'Embeddings':[['word2vec']],
                   'Embeddings+Unigrams':[['word2vec','unigrams']],
                   'Embeddings+Bigrams':[['word2vec','bigrams']],
                   'Embeddings+Unigram+Bigrams':[['word2vec','unigrams','bigrams']]}

other_features_map={'no social, no emotion': [[]],
                    'solo social (normalized?)': [['social']],
                    'solo emotion (emolex)':[['emolex']],
                    'solo emotion (hurtlex)':[['hurtlex']],
                    'social + emotion (emolex)':[['social','emolex']],
                    'social + emotion (hurtlex)':[['social','hurtlex']]}

emoji_feature_map={'NO':[False],
                   'emoji transcription': ['emoji-transcription'],
                   'vec emoji' : ['vec-emoji']}

recurrent_layer_map={'blstm': ['BiLSTM'],
                     'lstm': ['LSTM'],
                     'none': [],
                     'GRU': ['GRU']}


def get_field_name(field_map, field):
    return field_map[field]


def create_config_files(first_line, last_line):
    csvfile=open('all_configs.csv')
    csvreader=csv.DictReader(csvfile)
    os.makedirs('from_csv',exist_ok=True)
    for idx,line in enumerate(csvreader):
        curr_config=copy.deepcopy(base_config) #FIXME: get the default values
        curr_config['train_dataset']=get_field_name(dataset_map, line['TRAIN'])
        curr_config['test_dataset']=get_field_name(dataset_map, line['TEST'])
        curr_config['all_word2vec_sources']=get_field_name(word2vec_source_map,
                                                      line['EMBEDDINGS'])
        curr_config['all_text_features']=get_field_name(text_features_map,
                                                    line['TEXTFEATURES'])
        curr_config['all_other_features']=get_field_name(other_features_map,
                                                     line['OTHERFEATURES'])
        curr_config['all_emojis']=get_field_name(emoji_feature_map,
                                                 line['EMOJI'])

        curr_config['all_recurrent_layer_types']=get_field_name(recurrent_layer_map,
                                                           line['NETWORK'])

        curr_config['all_normalize_hashtags']=[line['HASTAG NORMALIZATION']=='YES']

        out_jsonfile=open('from_csv/{}.json'.format(idx),'w')
        json.dump(curr_config, out_jsonfile)
        out_jsonfile.close()




if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--first_line', action='store', default=0)
    parser.add_argument('--last_line', action='store', default=np.Inf)
    args=parser.parse_args()

    create_config_files(args.first_line, args.last_line)
