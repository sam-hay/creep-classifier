#!/usr/bin/env python3
import csv
import json
import argparse
import numpy as np
import copy
import os

base_config={'all_layer_sizes':[[500]],
             'all_recurrent_layer_sizes': [[200]],
             'all_lambdas' : [0],
             'all_batch_sizes' : [32],
             'all_dropouts' : [[]],
             'all_recurrent_dropouts' : [0],
             'all_use_batch_normalization' : [False],
             'all_model_types': ['nnet'],
             'all_recurrent_layer_types': ['GRU']
             }

dataset_map={'facebook':'evalita-facebook',
             'twitter': 'evalita-twitter',
             'instagram': 'instagram-ita2',
             'whatsapp': 'whatsapp',
             'whatsapp (?)': 'whatsapp',
             'twitter+facebook+instagram+whatsapp' : 'twitter+facebook+instagram+whatsapp',
             'twitter+instagram+whatsapp (solo messaggi < 280 caratteri, da calcolare dopo aver tolto i link e gli username)': 'twitter+facebook+instagram+whatsapp<280'}

word2vec_source_map={'fasttext_ita' : ['fasttext-it'],
                     'twitter_ita' : ['fasttext-twitter-it']}

text_features_map={'Embeddings':[['word2vec']]}

other_features_map={'no social, no emotion': [[]],
                    'solo social (normalized?)': [['social']],
                    'solo emotion (emolex)':[['emolex']],
                    'solo emotion (hurtlex)':[['hurtlex']],
                    'social + emotion (emolex)':[['social','emolex']],
                    'social + emotion (hurtlex)':[['social','hurtlex']]}

emoji_feature_map={'NO':[False],
                   'emoji transcription': ['emoji-transcription'],
                   'vec emoji' : ['vec-emoji']}

def get_field_name(field_map, field):
    return field_map[field]


def create_config_files(first_line, last_line):
    csvfile=open('Cross-Platform per Clic-it - Sheet1.csv','r')
    csvreader=csv.DictReader(csvfile)
    os.makedirs('from_csv_clicit',exist_ok=True)
    for idx,line in enumerate(csvreader):
        print(line)
        curr_config=copy.deepcopy(base_config)
        curr_config['train_dataset']=get_field_name(dataset_map, line['TRAIN'])
        curr_config['test_dataset']=get_field_name(dataset_map, line['TEST'])
        curr_config['all_word2vec_sources']=get_field_name(word2vec_source_map,
                                                      line['EMBEDDINGS'])
        curr_config['all_text_features']=get_field_name(text_features_map,
                                                    line['TEXTFEATURES'])
        curr_config['all_other_features']=get_field_name(other_features_map,
                                                     line['OTHERFEATURES'])
        curr_config['all_emojis']=get_field_name(emoji_feature_map,
                                                 line['EMOJI'])

        curr_config['all_normalize_hashtags']=[line['HASTAG NORMALIZATION']=='YES']

        out_jsonfile=open('from_csv_clicit/{}.json'.format(idx),'w')
        json.dump(curr_config, out_jsonfile)
        out_jsonfile.close()




if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--first_line', action='store', default=0)
    parser.add_argument('--last_line', action='store', default=np.Inf)
    args=parser.parse_args()

    create_config_files(args.first_line, args.last_line)
