# Creep Classifier

Classify cyberbullying on social networks

### Prerequisites

For this to run, the following python libraries are needed:

* numpy
* keras
* tensorflow
* nltk
* sklearn
* h5py
* matplotlib
* emoji
* spacy
* colorama
* fasttext

To install them, both pip and conda can be used. To install packages using pip, run the following command:

```
pip install -r requirements.txt
```

For conda, we provide an ```environment.yml``` file, that creates an environment called ```creep-cyberbullying-classifier```.
Unfortunately, since the last version of the fasttext bindings for python is not provided neither by the default nor the conda-forge channels, you will need to install it manually.
To install all required packages, use:

```
conda env create -f environment.yml
pip install fasttext
```

In order to install the required modules from spacy for the supported languages, use:

```
python -m spacy download en
python -m spacy download it
python -m spacy download de
```

In order to download the VaderSentiment lexicon, needed for the sentiment feature, run ```python``` and type:

```
>>> import nltk
>>> nltk.download('vader_lexicon')
```

## Running the model

To run the model (by default it will train first, then select the best model and test) just:

```
cd src
./creep_classifier.py
```
For more advanced options, see:
```
./creep_classifier.py -h
```

## Hyperparameter configuration

All the hyperparameters that are used in the grid search by the classifier are stored in a config file, see the default one in `config/default_config.py`. A different file can be specified by
using the `--config` option when launching the classifier. See the [config readme](config/Readme.md) for more details.

## Datasets and Embeddings

See the [data readme](data/Readme.md).

