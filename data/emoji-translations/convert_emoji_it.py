#!/usr/bin/env python3
import csv
import re
import emoji
import pickle

emoji_tsv=open('emoji_ita.tsv','r')
csvreader=csv.reader(emoji_tsv, delimiter='\t', quoting=csv.QUOTE_NONE)

remove_spaces_re=re.compile(r'[:]* ')
remove_quotes_re=re.compile(r'["“,”]+')
fix_flags_re=re.compile(r'flag_([A-Z])')

en2it_emojis={}

for line in csvreader:
    formatted_eng=remove_spaces_re.sub('_', line[2])
    formatted_eng=remove_quotes_re.sub('', formatted_eng)
    #formatted_eng=fix_flags_re.sub(r'flag_for_\1',formatted_eng)
    formatted_eng=":{}:".format(formatted_eng)
    if formatted_eng not in emoji.EMOJI_UNICODE:
        print(formatted_eng)
    en2it_emojis[formatted_eng]=" "+line[3]+" "

emoji_tsv.close()

dict_file=open('en2it','wb')
pickle.dump(en2it_emojis, dict_file)
dict_file.close()
