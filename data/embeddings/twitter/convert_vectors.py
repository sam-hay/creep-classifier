import h5py
from word2vecReader import Word2Vec
import re
import sys

def escape_word(word):
    if(len(word)==0):
        word='__emptystr__'
    elif(word=='.'):
        word='__dot_symbol__'
    elif('/' in word):
        word=re.sub('\/', '__slash_symbol__', word)
    return word

h5py_file=h5py.File('vectors.hdf5','w')
model_path = "word2vec_twitter_model.bin"
embeddings = Word2Vec.load_word2vec_format(model_path, binary=True)
print('loaded embeddings from file')
vector_group=h5py_file.create_group('vectors')
i=0
total=len(embeddings.vocab)
for word in embeddings.vocab:
    vector=embeddings.syn0[embeddings.vocab[word].index]
    word=escape_word(word) 
    sys.stdout.write('\rparsing embedding {} of {}'.format(i,total))
    sys.stdout.flush()
    curr_word=vector_group.create_dataset(word, shape=vector.shape)
    curr_word[:]=vector
    i+=1

h5py_file.close()
    
