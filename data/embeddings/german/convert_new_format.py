#!/usr/bin/env python3

import h5py

infile=h5py.File('embeddings.hdf5','r')
outfile=h5py.File('embeddings_new.hdf5')

infile.copy(infile['vectors'],dest=outfile,name='embeddings')

infile.close()
outfile.close()
