## Word2vec embeddings

The `twitter` and `google` embeddings are downloaded and converted automatically by using:
```
./download_and_convert.sh
```
in the appropriate folder.

## Single language embeddings

Embeddings in the gensim format have to be converted by using the python script in the `utils` folder as follows:
```
./convert_to_h5py input_filename desired_embedding_dir/embeddings.hdf5
```
where `desired_embedding_dir` is either german or italian

## Multiple language embeddings

Multilingual Embeddings in the gensim format have to be converted by using the python script in the `utils` folder as follows:
```
./convert_h5py_multilingual.sh input_directory desired_embedding_dir/embeddings.hdf5 [--italian] [--english] [--german] [--emoji]
```
Specifying the available languages. The embedding files need to have the following format:
```
vec_lang.txt
```
Where lang is one of it,de,en,emoji.

The system supports three different embedding types, expecting the following languages:

* multilingual: italian and english;
* multilingual-ger: german and english
* multilingual-pivot: german, italian, english, emoji

## Fasttext embeddings

A single script is provided to download, extract and rename all official fasttext embeddings:

```
cd fasttext
./download_fasttext.sh
```

