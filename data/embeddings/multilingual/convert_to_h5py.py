#!/usr/bin/env python3
import csv
import h5py
import numpy as np
import sys
import re

def escape_word(word):
    if(len(word)==0):
        word='__emptystr__'
    elif(word=='.'):
        word='__dot_symbol__'
    elif('/' in word):
        word=re.sub('\/', '__slash_symbol__', word)
    return word

n_words={}

vector_h5py=h5py.File('embeddings.hdf5','w')

file_pattern='vectors_{}_aligned_scraping_v2.txt'


for language in ['it','en']:
    print('reading {} words'.format(language))
    
    file_name=file_pattern.format(language)
    lang_group=vector_h5py.create_group(language)
    vfile=open(file_name)
    header=vfile.readline()
    header=header.split(' ')
    n_words[language]=int(header[0])
    for l_idx,line in enumerate(vfile):
        sys.stdout.write('\rconverting vector {} of {}'.format(l_idx, n_words[language]))
        row=line.split(" ")
        word=escape_word(row[0])
        vector=np.array(row[1:-1], dtype=np.float)
        try:
            lang_group[word]=vector
        except:
            print('\n')
            print('error')
            print(word)
            sys.exit()
    sys.stdout.write('\n')
    sys.stdout.flush()

vector_h5py.close()
        
