#!/usr/bin/env python
import h5py
from scipy.spatial.distance import cosine
import sys
import numpy as np

word=sys.argv[1]
vectors=h5py.File('vectors.hdf5','r')

word_vec=vectors['it'][word]

closest_distance=np.inf
closest_word='no_word'
for en_word in vectors['en']:
    curr_vec=vectors['en'][en_word]
    curr_distance=cosine(word_vec, curr_vec)
    if(curr_distance < closest_distance):
        print('{} is closer than {}'.format(en_word,closest_word))
        closest_distance=curr_distance
        closest_word=en_word

print(en_word)
