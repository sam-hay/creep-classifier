#!/usr/bin/python
import csv
import h5py
import numpy as np
import sys
import re
import argparse

def escape_word(word):
    if(len(word)==0):
        word='__emptystr__'
    elif('\0' in word):
        word=re.sub('\0', '__nullstring__', word)
    elif(word=='.'):
        word='__dot_symbol__'
    elif('/' in word):
        word=re.sub('\/', '__slash_symbol__', word)
    return word


def convert_h5py(input_filename, output_filename):
    embeddings_h5py=h5py.File(output_filename)
    lang_group=vector_h5py.create_group('embeddings')
    vfile=open(input_filename)
    header=vfile.readline()
    header=header.split(' ')
    n_words=int(header[0])
    for l_idx,line in enumerate(vfile):
        sys.stdout.write('\rconverting vector {} of {}'.format(l_idx, n_words))
        row=line.split(" ")
        word=escape_word(row[0])
        vector=np.array(row[1:], dtype=np.float)
        try:
            lang_group[word]=vector
        except:
            print('\n')
            print('error')
            print(word)
            print(len(word))
            sys.exit()
    sys.stdout.write('\n')
    sys.stdout.flush()

    vector_h5py.close()


if(__name__=='__main__'):
    parser=argparse.ArgumentParser(description='convert single language word embeddings to hdf5')
    parser.add_argument('input_filename', action='store')
    parser.add_argument('output_filename', action='store')
    parser.parse_args()

    convert_h5py(args.input_filename, args.output_filename)

