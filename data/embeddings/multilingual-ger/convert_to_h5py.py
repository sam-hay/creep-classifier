#!/usr/bin/env python3
import csv
import json
import numpy as np
import h5py
import sys
import re
import sys


def convert_to_h5py(inputfile, outputfile,language):
    from preprocess.text_normalization import escape_word_h5py,decode_emoji
    n_words={}

    print('reading {} words'.format(language))
    
    file_name=file_pattern.format(language)
    lang_group=outputfile.create_group(language)
    if(language=='emoji'):
        n_words=856
        emoji_file=open('emojis_format.json')
        emoji_dict=json.load(emoji_file)
        emoji_file.close()
    else:
        header=inputfile.readline()
        header=header.split(' ')
        n_words=int(header[0])
    for l_idx,line in enumerate(inputfile):
        sys.stdout.write('\rconverting vector {} of {}'.format(l_idx, n_words))
        row=line.split(" ")
        if(language=='emoji'):
            word=decode_emoji(row[0], emoji_dict)
            if(word is None):
                continue
        else:
            word=escape_word_h5py(row[0])
        vector=np.array(row[1:], dtype=np.float)
        try:
            lang_group[word]=vector
        except:
            print('\n')
            print('error')
            print(word)
            sys.exit()

    sys.stdout.write('\n')
    sys.stdout.flush()



if(__name__=='__main__'):
    sys.path.append('../../../src')
    file_pattern='vec_{}_both.txt'
    dest_file=h5py.File('embeddings.hdf5','w')
    for language in ['en','de']:
        inputfile=open(file_pattern.format(language))
        convert_to_h5py(inputfile,dest_file,language)
        inputfile.close()
    
    inputfile=open('vec_emoji_aligned_en.txt')
    convert_to_h5py(inputfile, dest_file, 'emoji')

    dest_file.close()
