#!/usr/bin/env python3 
import csv
import h5py
import numpy as np
import sys
import re

def escape_word(word):
    if(len(word)==0):
        word='__emptystr__'
    elif(word=='.'):
        word='__dot_symbol__'
    elif('/' in word):
        word=re.sub('\/', '__slash_symbol__', word)
    return word

n_words={}

vector_h5py=h5py.File('embeddings.hdf5','w')

file_name='embed_tweets_de_300D_fasttext'

lang_group=vector_h5py.create_group('embeddings')
vfile=open(file_name)
for l_idx,line in enumerate(vfile):
    sys.stdout.write('\rconverting vector {}'.format(l_idx))
    row=line.split(" ")
    word=escape_word(row[0])
    vector=np.array(row[1:-1], dtype=np.float)
    try:
        lang_group[word]=vector
    except:
        print('\n')
        print('error')
        print(word)
        sys.exit()
sys.stdout.write('\n')
sys.stdout.flush()

vector_h5py.close()
        
