#!/usr/bin/env python3

import gensim
import re
import csv

class DocumentIterator:
    def __init__(self):
        self.i=0
        self.n=196985692
        self.file=open('twitter-sample/join_files.txt')

    def __iter__(self):
        return self

    def __next__(self):
        if (self.i< self.n):
            i=self.i
            self.i+=1
            line=self.file.readline()
            line_split=re.split(r'\t', line)
            return gensim.models.doc2vec.TaggedDocument(gensim.utils.simple_preprocess(line_split[1]), i)
        else:
            raise StopIteration

def document_iterator():
    folder='twitter-sample/'
    file_list=['join_files.txt']
    for f in file_list:
        fname=folder+f
        with open(fname, 'r') as tsvfile:
            for i,line in enumerate(tsvfile):
                line_split=re.split(r'\t+',line)
                print(line_split)
                yield gensim.models.doc2vec.TaggedDocument(gensim.utils.simple_preprocess(line_split[1]), i)
    raise StopIteration


train_corpus=gensim.models.doc2vec.TaggedLineDocument('./twitter-sample/test-smaller.txt')
print('corpus ok')

model=gensim.models.doc2vec.Doc2Vec(vector_size=200, min_count=2, epochs=100, workers=4)
print('model init ok')

model.build_vocab(train_corpus)
print('build vocab ok')

train_corpus=gensim.models.doc2vec.TaggedLineDocument('./twitter-sample/test-smaller.txt')

model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)

print(model.infer_vector('you suck'))

model.save('test-model')
