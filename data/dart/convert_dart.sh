#!/bin/bash

for FILE in *.json
do
	if [[ $FILE != *"_ann"* ]]; then
		echo $FILE;
		BASENAME=$(echo "$FILE" | cut -d"." -f1)
		FILE_LINES=$(wc -l "$FILE"|cut -d' ' -f1)
		perl -pe 's/\n/,\n/ if$.<'$FILE_LINES';s/^/\[/ if$. ==1;s/\n/\]/ if$.=='$FILE_LINES $FILE > $BASENAME'_ann.json'
	fi
done
