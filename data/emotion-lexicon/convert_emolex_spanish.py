import csv
import h5py
import numpy as np

categories2idx = {
    'Positive': 0,
    'Negative': 1,
    'Anger': 2,
    'Anticipation': 3,
    'Disgust': 4,
    'Fear': 5,
    'Joy': 6,
    'Sadness': 7,
    'Surprise': 8,
    'Trust': 9
}

all_words = {}

def convert_emolex():
    h5py_file = h5py.File('lexicon.hdf5', 'r+')
    infile = open('emolex-full.csv', 'r')

    es_group = h5py_file.create_group('es')
    reader = csv.DictReader(infile, delimiter=',')
    for row in reader:

        word = row['Spanish (es)']
        values = np.zeros((10,))

        for category in categories2idx:
            idx = categories2idx[category]
            values[idx] = row[category]

        if word in all_words:
            all_words[word] = np.maximum(values, all_words[word])
        else:
            all_words[word] = values

    for word, values in all_words.items():
        es_group[word] = values

    h5py_file.close()
    infile.close()

if __name__ == '__main__':
    convert_emolex()
