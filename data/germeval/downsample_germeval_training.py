#!/usr/bin/env python3

import csv
import numpy as np
import pickle
from sklearn.model_selection import train_test_split


"""764 1
1636 0"""

negative_target=1636
positive_target=764


csvfile=open('germeval2018.training.txt', 'r')
reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
data=[]
for line in reader:
    post={}
    post['text']=line[0]
    post['abuse']=int(line[2]=='ABUSE')
    post['insult']=int(line[2]=='INSULT')
    post['profanity']=int(line[2]=='PROFANITY')
    post['other']=int(line[2]=='OTHER')

    post['offensive']=int(line[1]=='OFFENSE')

    post['language']='de'
    data.append(post)


data=np.asarray(data)

train_data, val_data=train_test_split(data, test_size=0.2, random_state=41)

negative_instances=[]
abuse_instances=[]
insult_instances=[]
profanity_instances=[]

for post in train_data:
    if(post['abuse']==1):
        abuse_instances.append(post)
    elif(post['insult']==1):
        insult_instances.append(post)
    elif(post['profanity']==1):
        profanity_instances.append(post)
    else:
        negative_instances.append(post)




total_positives=len(abuse_instances)+len(insult_instances)+len(profanity_instances)
fraction_abuse=len(abuse_instances)/total_positives
fraction_insult=len(insult_instances)/total_positives
fraction_profanity=len(profanity_instances)/total_positives

print(total_positives)
print(fraction_abuse)
print(fraction_insult)
print(fraction_profanity)

assert fraction_abuse + fraction_insult + fraction_profanity ==1

abuse_target=round(fraction_abuse*positive_target)
insult_target=round(fraction_insult*positive_target)
profanity_target=round(fraction_profanity*positive_target)

print(abuse_target)
print(insult_target)
print(profanity_target)

assert abuse_target + insult_target + profanity_target == positive_target

negative_instances=np.asarray(negative_instances)

abuse_instances=np.asarray(abuse_instances)
insult_instances=np.asarray(insult_instances)
profanity_instances=np.asarray(profanity_instances)

np.random.seed(42)
print('neg before shuffle!')
print(negative_instances)
np.random.shuffle(negative_instances)
print('neg after shuffle!')
print(negative_instances)
negative_instances=negative_instances[:negative_target]

print('abuse before shuffle!')
print(abuse_instances)
np.random.shuffle(abuse_instances)
print('abuse after shuffle!')
print(abuse_instances)
abuse_instances=abuse_instances[:abuse_target]

print('insult before shuffle!')
print(insult_instances)
np.random.shuffle(insult_instances)
print('insult after shuffle!')
print(insult_instances)
insult_instances=insult_instances[:insult_target]

print('profanity before shuffle!')
print(profanity_instances)
np.random.shuffle(profanity_instances)
print('profanity after shuffle!')
print(profanity_instances)
profanity_instances=profanity_instances[:profanity_target]

print(negative_instances.shape)
print(abuse_instances.shape)
print(insult_instances.shape)
print(profanity_instances.shape)


resampled=np.concatenate((negative_instances, abuse_instances, insult_instances,
                          profanity_instances))

print(resampled.shape)

output_resampled=open('resampled.pickle', 'wb')
pickle.dump(resampled,output_resampled)
output_resampled.close()
