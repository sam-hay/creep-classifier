#!/usr/bin/env python

""" 
Sample code of getting tweet JSON objects by tweet ID lists.

You have to install tweepy (This script was tested with Python 2.6 and Tweepy 3.3.0)
https://github.com/tweepy/tweepy
and set its directory to your PYTHONPATH. 

You have to obtain an access tokens from dev.twitter.com with your Twitter account.
For more information, please follow:
https://dev.twitter.com/oauth/overview/application-owner-access-tokens

Once you get the tokens, please fill the tokens in the squotation marks in the
following "Access Information" part. For example, if your consumer key is 
LOVNhsAfB1zfPYnABCDE, you need to put it to Line 33
consumer_key = 'LOVNhsAfB1zfPYnABCDE' 



"""

# call user.lookup api to query a list of user ids.
import twitter
import sys
import codecs
import csv
import json
from time import sleep

####### Access Information #################

# Parameter you need to specify
consumer_key = '4DbpKZgNa0x05PcZsgc7xIH8h'
consumer_secret = 'ziPiNo9Kjg5NtNkiSOd7GYthg4YQD4VuibSWUvaa3VkmVrTNeP'
access_key = '960426216451919872-Az1uRd85On6LuoyRibbnw1iS8bdaHIO'
access_secret = 'wFweCxzFmpwDTBhE4BoHCiui8vpv0I1T3BAKlGcnsnnVZ'

inputFile = 'data.csv'

#############################################

api=twitter.Api(consumer_key=consumer_key,
                       consumer_secret=consumer_secret,
                       access_token_key=access_key,
                       access_token_secret=access_secret,
                       timeout=60, sleep_on_rate_limit=True)

l=[]
user_timelines=[]
user_infos=[]
tweets=[]
i=0

tweet_file=open('tweets.json','w')
user_file=open('users.json','w')
timeline_file=open('timelines.json','w')

with open(inputFile, 'r') as csvfile:
    spamreader = csv.reader(csvfile)
    for line in spamreader:
        print('line number '+str(i))
        l.append(line)
        if (len(l)>=50):
            tweet_ids=[x[0] for x in l]
            tweets=[]
            users=[]
            timelines=[]
            for t in tweet_ids:
                while (True):
                    try:
                        tweet=api.GetStatus(t)
                        tweets.append(tweet)
                        print('tweet found')
                        print(tweet)
                        user=api.GetUser(tweet.user.id)
                        users.append(user)
                        print('user found')
                        print(user)
                        timeline=api.GetUserTimeline(user_id=tweet.user.id , max_id=t)
                        timelines.append(timeline)
                        print('timeline found')
                        print(timeline)
                        break
                    except twitter.error.TwitterError as e:
                        print('tweet not found')
                        print(e)
                        break
                    except:
                        sleep(60)
                        print('try again')
        i+=1
