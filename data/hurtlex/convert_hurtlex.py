#!/usr/bin/env python3

import argparse
import csv
import h5py
import numpy as np

categories2idx={'an':0,'asf':1,'asm':2,'cds':3,'ddf':4,'ddp':5,'dmc':6,'is':7,
                'om':8,'or':9,'pa':10,'pr':11,'ps':12,'qas':13,'rci':14,'re':15,
                'svp':16}

all_words={}

def convert_hurtlex():
    h5py_file=h5py.File('lexicon.hdf5', 'w')

    for lang in ['en','de','es']:
        infile='hurtlex_{}_conservative.tsv'.format(lang.upper())
        input_file = open(infile,'r')
        lang_group=h5py_file.create_group(lang)
        reader = csv.reader(input_file, delimiter='\t', quoting=csv.QUOTE_NONE)

        for line in reader:
            fine_grained_category=line[0]
            stereotype_macro=line[1]=='yes'
            word=line[2]

            values_hurtlex=np.zeros((19,))
            values_hurtlex[categories2idx[fine_grained_category]]=1
            if(stereotype_macro):
                values_hurtlex[17]=1
            else:
                values_hurtlex[18]=1

            if(word in all_words):
                all_words[word] = np.maximum(values_hurtlex,all_words[word])
            else:
                all_words[word] = values_hurtlex

        for word,values in all_words.items():
            lang_group[word]=values

        input_file.close()
    h5py_file.close()



if __name__ == '__main__':
    convert_hurtlex()
