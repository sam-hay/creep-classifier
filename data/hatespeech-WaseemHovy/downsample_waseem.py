#!/usr/bin/env python

import csv
import numpy as np
import pickle
from sklearn.model_selection import train_test_split

"""2028 0
972 1"""

negative_target=1636
positive_target=764


from random import shuffle
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS

with open('NAACL_SRW_2016_tweets.txt','r') as csvfile:
    data=[]
    sentiment_analyzer=VS()
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    for row in reader:
        tweet={'text':row[2]}
        tweet['language']='en'
        tweet['racism']=int(row[1]=='racism')
        tweet['sexism']=int(row[1]=='sexism')
        tweet['none']=int(row[1]=='none')
        tweet['hate-speech']=int(row[1]=='racism' or row[1]=='sexism')
        sentiment = sentiment_analyzer.polarity_scores(tweet['text'])
        tweet['sentiment']=float(sentiment['compound'])
        data.append(tweet)

train_data,test_data=train_test_split(data, test_size=0.4, random_state=42)

racism_instances=[]
sexism_instances=[]
negative_instances=[]

for tweet in train_data:
    if(tweet['racism']==1):
        racism_instances.append(tweet)
    elif(tweet['sexism']==1):
        sexism_instances.append(tweet)
    else:
        negative_instances.append(tweet)

total_positives=len(sexism_instances)+len(racism_instances)
fraction_sexism=len(sexism_instances)/total_positives
fraction_racism=len(racism_instances)/total_positives

print(total_positives)
print(fraction_sexism)
print(fraction_racism)

assert fraction_racism + fraction_sexism ==1

racism_target=round(fraction_racism*positive_target)
sexism_target=round(fraction_sexism*positive_target)

print(racism_target)
print(sexism_target)

assert racism_target + sexism_target == positive_target


negative_instances=np.asarray(negative_instances)
racism_instances=np.asarray(racism_instances)
sexism_instances=np.asarray(sexism_instances)

np.random.seed(42)
print('neg before shuffle!')
print(negative_instances)
np.random.shuffle(negative_instances)
print('neg after shuffle!')
print(negative_instances)
negative_instances=negative_instances[:negative_target]

print('racism before shuffle!')
print(racism_instances)
np.random.shuffle(racism_instances)
print('racism after shuffle!')
print(racism_instances)
racism_instances=racism_instances[:racism_target]

print('sexism before shuffle!')
print(sexism_instances)
np.random.shuffle(sexism_instances)
print('sexism after shuffle!')
print(sexism_instances)
sexism_instances=sexism_instances[:sexism_target]

print(negative_instances.shape)
print(racism_instances.shape)
print(sexism_instances.shape)


resampled=np.concatenate((negative_instances, racism_instances, sexism_instances))

print(resampled.shape)

output_resampled=open('resampled.pickle', 'wb')
pickle.dump(resampled,output_resampled)
output_resampled.close()
